<?php

namespace App\Http\Controllers;


use App\Photo;
use App\Flyer;
use App\AddPhotoToFlyer;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AddPhotoRequest;
use App\Http\Controllers\Controller;

class PhotosController extends Controller
{
    public function store($zip, $street, AddPhotoRequest $request)
    {

        $flyer = Flyer::LocatedAt($zip, $street);
        $photo = $request->file('photo');
        (new AddPhotoToFlyer($flyer, $photo))->save();

    }
}
