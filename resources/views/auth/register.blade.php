@extends('layout')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <h1>Register</h1>
        <form method="POST" action="/auth/register">

            {{csrf_field()}}

            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" class="form-control" value="{{old('name')}}" required>
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" class="form-control" value="{{old('email')}}" required>
            </div>

            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control" value="{{old('password')}}" required>
            </div>

            <div class="form-group">
                <label for="password_confirmation">Confirm Password:</label>
                <input type="password" name="password_confirmation" class="form-control" value="{{old('password_confirmation')}}" required>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-default">Register</button>
            </div>

        </form>

    </div>

@stop